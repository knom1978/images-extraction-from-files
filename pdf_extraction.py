#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import PyPDF2
from PIL import Image
from datetime import datetime, timezone, timedelta
import os
import sys

mypath = '/app/folder'
filename_list = []

for file in os.listdir(mypath):
    if file.endswith('.pdf'):
        filename_list.append(file)
        print(file)

if not os.path.exists(str(mypath + '/images')):
    os.makedirs(str(mypath + '/images'))

def ExtractImages(filename):
    print("\n---------------------------------------")
    print("This is the pdf processing",filename)
    fileObject = PyPDF2.PdfFileReader(open(str(mypath + '/' + filename), "rb"))
    print(fileObject)
    pages = fileObject.getNumPages()
    print("Total number of Pages is.....",pages)
    filename_str = mypath +'/images/' + datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(timezone(timedelta(hours=8))).strftime("%Y%m%d%H%M%S-") + filename +'_'
    for i in range(0,pages):
        tempPage = fileObject.getPage(i)
        if '/XObject' in tempPage['/Resources']:
            xObject = tempPage['/Resources']['/XObject'].getObject()
            for obj in xObject:
                if xObject[obj]['/Subtype'] == '/Image':
                    size = (xObject[obj]['/Width'], xObject[obj]['/Height'])
                    data = xObject[obj].getData()
                    if xObject[obj]['/ColorSpace'] == '/DeviceRGB':
                        mode = "RGB"
                    else:
                        mode = "P"
                    if '/Filter' in xObject[obj]:
                        if xObject[obj]['/Filter'] == '/FlateDecode':

                            img = Image.frombytes(mode, size, data)
                            img.save(filename_str + obj[1:] + ".png")
                        elif xObject[obj]['/Filter'] == '/DCTDecode':
                            img = open(filename_str + obj[1:] + ".jpg", "wb")
                            img.write(data)
                            img.close()
                        elif xObject[obj]['/Filter'] == '/JPXDecode':
                            img = open(filename_str + obj[1:] + ".jp2", "wb")
                            img.write(data)
                            img.close()
                        elif xObject[obj]['/Filter'] == '/CCITTFaxDecode':
                            img = open(filename_str + obj[1:] + ".tiff", "wb")
                            img.write(data)
                            img.close()
                    else:
                        img = Image.frombytes(mode, size, data)
                        img.save(filename_str + obj[1:] + ".png")
        else:
            print("No image found for file.",filename)

for i in filename_list:
    ExtractImages(i)
