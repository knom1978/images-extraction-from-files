Images extraction from file
===
###### tags: `python` `docx` `xlsx` `pptx` `pdf`
## Introduction
The demo for images extracrion from docx, xlsx, pptx and pdf files.

## Method
* 使用python的zipfile and pypdf2 librarys.

## Usage
To git clone this, and docker build
```
docker build -t cecil/img_extraction:1.0 .
docker run -it --rm -v "$PWD":/app/folder cecil/img_extraction:1.0 python3 zip_extraction.py
docker run -it --rm -v "$PWD":/app/folder cecil/img_extraction:1.0 python3 pdf_extraction.py
```
