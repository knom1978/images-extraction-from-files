#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import zipfile
from datetime import datetime, timezone, timedelta
import os
import sys

mypath = '/app/folder'
file_end_list = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'tiff']
doc_end_list = ['docx', 'xlsx', 'pptx']

doc_path_list = []
filename_list = []

for file in os.listdir(mypath):
    for suffix in (suffix for suffix in doc_end_list if file.endswith(suffix)):
        doc_path_list.append(os.path.join(mypath, file))
        filename_list.append(file)

#file_name_list = ['NTHU_DOCX.docx', 'NTHU_XLSX.xlsx', 'NTHU_PPTX.pptx']
def exctact_docx_xlsx_pptx_imgs(file_name):
    for i in zipfile.ZipFile(str(mypath +'/' +file_name), 'r').infolist():
        i.filename = datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(timezone(timedelta(hours=8))).strftime("%Y%m%d%H%M%S-") + file_name + i.filename.replace("/", "-")
        for suffix in (suffix for suffix in file_end_list if str(i.filename).endswith(suffix)):
            zipfile.ZipFile(str(mypath +'/' +file_name), 'r').extract(i, path=str(mypath + '/images'))

for i in filename_list:
    exctact_docx_xlsx_pptx_imgs(i)
