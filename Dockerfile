FROM ubuntu:18.04
MAINTAINER Cecil Liu "dcecil_liu@umc.com"

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
  && apt-get install -y git tzdata python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

RUN echo Asia/Taipei” > /etc/timezone
RUN dpkg-reconfigure -f noninteractive tzdata

# install requirements
# https://blog.csdn.net/kmesky/article/details/103187833
RUN pip3 install Pillow git+https://github.com/mstamy2/PyPDF2.git

COPY zip_extraction.py /app/zip_extraction.py
COPY pdf_extraction.py /app/pdf_extraction.py
WORKDIR /app

CMD ["python3"]
